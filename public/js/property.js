
    $(document).ready(function(){
        function init() {
            $("#properties_table").hide();
            $("#download_btn").hide();

            const queryProperties = "?page=1";

            $.ajax({
                url: `/api/properties${queryProperties}`,
                success: function(result) {
                    buildTable(result);
                    $("#loading_message").hide();
                    $("#properties_table").data("last-query", queryProperties);
                    $("#properties_table").show();
                    $("#download_btn").show();
                }
            });

            $("#properties_table > thead").click(function(element) {
                const field = $(element.target).text();
                const sign = toggleSortOrder($(element.target).data("sort-order"));
                const queryProperties = `?sortBy=${encodeURIComponent(sign)}${field}&page=1`;

                $.ajax({
                    url: `/api/properties${queryProperties}`,
                    success: function(result) {
                        buildTable(result);
                        $("#properties_table").data("last-query", queryProperties);
                        $(element.target).data("sort-order", sign);
                    }
                });
            });

            $("#download_btn").click(function () {
                console.log($("#properties_table").data("last-query"));
                const lastQuery = $("#properties_table").data("last-query");

                $.ajax({
                    url: `/api/download${lastQuery}`,
                    success: function() {
                        window.location.href = '/downloads/properties.json';
                    }
                });
            })
        }

        function buildTable(result) {
            const tableRows = result.map( property => {
                var image_url = property.image;
                if(image_url == "") {
                    image_url = "images/not-available.png";
                }
                return `<tr>
                        <td scope="row" class="text-nowrap">${property.id}</td>
                        <td class="text-nowrap">${property.city}</td>                        
                        <td>${property.title}</td>
                        <td><a href="${image_url}"><img src="${image_url}" class="img-thumbnail"></a></td>
                        <td class="text-nowrap"><a href="${property.link}" target="_blank">${property.link}</a></td>
                    `
            }).join('</tr>');

            $("#properties_table").find("tbody").html(tableRows);
        }

        function toggleSortOrder(order) {
            if (order === "+") {
                return "-";
            }

            return "+";
        }

        init();
    })
<?php
declare(strict_types=1);

namespace App\Repository;

use App\Domain\ValueObject\SortBy;

interface PropertyRepository
{
    public function filter(SortBy $sortBy, ?int $page, ?int $pageSize): array;
    public function save(array $properties): bool;
}
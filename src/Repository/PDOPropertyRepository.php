<?php
declare(strict_types=1);

namespace App\Repository;


use App\Domain\Entity\Property;
use App\Domain\ValueObject\SortBy;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class PDOPropertyRepository implements PropertyRepository
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->entityRepository = $this->entityManager->getRepository(Property::class);

    }

    public function filter(SortBy $sortBy, ?int $page, ?int $pageSize): array
    {
        $queryBuilder = $this->entityRepository->createQueryBuilder('property');

        if(null !== $sortBy->field()) {
            $queryBuilder->orderBy('property.' . $sortBy->field(), $sortBy->order());
        }

        if (null !== $page && null !== $pageSize) {
            $queryBuilder->setFirstResult(($page-1)*$pageSize);
        }

        return $queryBuilder
        ->setMaxResults($pageSize)
        ->getQuery()
        ->getResult();
    }

    public function save(array $properties): bool
    {
        foreach ($properties as $property) {
            $this->entityManager->persist($property);
            $this->entityManager->flush($property);
        }

        return true;
    }
}
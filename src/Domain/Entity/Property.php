<?php
declare(strict_types=1);

namespace App\Domain\Entity;

final class Property implements \JsonSerializable
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $link;
    /**
     * @var string
     */
    private $city;
    /**
     * @var string
     */
    private $image;

    private function __construct(int $id, string $title, string $link, string $city, string $image)
    {
        $this->id = $id;
        $this->title = $title;
        $this->link = $link;
        $this->city = $city;
        $this->image = $image;
    }

    public static function fromArray(array $property): self
    {
        return new self(
            (int) $property['id'],
            $property['title'],
            $property['link'],
            $property['city'],
            $property['image']
        );
    }

    public static function create(int $id, string $title, string $link, string $city, string $image): self
    {
        return new self($id, $title, $link, $city, $image);
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function link(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function city(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function image(): string
    {
        return $this->image;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'link' => $this->link,
            'city' => $this->city,
            'image' => $this->image,
        ];
    }
}

<?php
declare(strict_types=1);

namespace App\Domain\DataTransferObject;

final class FilterParametersDTO
{
    private const PAGE_SIZE = 30;

    /**
     * @var null|string
     */
    private $sortBy;
    /**
     * @var int|null
     */
    private $pageNumber;
    /**
     * @var int|null
     */
    private $pageSize;

    public function __construct(?string $sortBy, ?int $pageNumber, ?int $pageSize = null)
    {
        $this->sortBy = $sortBy;
        $this->pageNumber = $pageNumber;
        $this->pageSize = !is_null($pageSize) ? $pageSize : self::PAGE_SIZE;
    }

    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    public function getSortBy(): ?string
    {
        return $this->sortBy;
    }
}

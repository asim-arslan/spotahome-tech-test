<?php
declare(strict_types=1);

namespace App\Domain\ValueObject;

final class SortBy
{
    private const ORDER = [
        '+' => 'ASC',
        '-' => 'DESC'
    ];

    /**
     * @var null|string
     */
    private $field;
    /**
     * @var null|string
     */
    private $order;

    private function __construct(?string $field, ?string $order)
    {
        $this->field = !is_null($field) ? strtolower($field) : $field;
        $this->order = !is_null($order) ? strtolower($order) : $order;
    }

    public static function create(?string $sortBy): self
    {
        if (null !== $sortBy) {
            $order = self::convertToOrder($sortBy);
            $field = substr($sortBy, 1);

            return new self($field, $order);
        }

        return new self(null, null);
    }

    /**
     * @return null|string
     */
    public function field(): ?string
    {
        return $this->field;
    }

    /**
     * @return null|string
     */
    public function order(): ?string
    {
        return $this->order;
    }

    private static function convertToOrder(string $sortBy): string
    {
        return self::ORDER[$sortBy[0]];
    }
}
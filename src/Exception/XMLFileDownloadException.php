<?php
declare(strict_types=1);

namespace App\Exception;

class XMLFileDownloadException extends \RuntimeException
{
    private $fileName;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;

        parent::__construct($this->errorMessage(), $this->errorCode());
    }

    private function errorCode(): string
    {
        return 'download_file_error';
    }

    private function errorMessage(): string
    {
        return sprintf('XML file download failed', $this->fileName);
    }
}
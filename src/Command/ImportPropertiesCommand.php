<?php
declare(strict_types=1);

namespace App\Command;

use App\Services\ImportProperties;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Exception\XMLFileDownloadException;


class ImportPropertiesCommand extends Command
{
    CONST XML_FILE_NAME = "PropertyData.xml";
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ImportProperties
     */
    private $importService;
    /**
     * @var string
     */
    private $url;

    public function __construct(
        LoggerInterface $logger,
        ImportProperties $importService,
        string $url
    )
    {
        parent::__construct();
        $this->logger = $logger;
        $this->importService = $importService;
        $this->url = $url;
    }

    protected function configure(): void
    {
        $this->setName('app:properties:import')
            ->setDescription('Imports properties from XML file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $archiveDestPath = __DIR__ . "/../data/";

        $this->logger->info('Downloading XML File..');
        $this->downloadData($output, $archiveDestPath);
        $this->logger->info('Download Complete..');

        $this->logger->info('Starting import..');
        $this->importService->execute($archiveDestPath.self::XML_FILE_NAME);
        $this->logger->info('Import finished!');
    }

    private function downloadData(OutputInterface &$output, string $archiveDestPath): void {
        //Disable temporary SSL verification (IMPROVE THAT)
        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            )
        );

        //Create ProgressBar
        $progress = new ProgressBar($output);

        //Create Stream Context with Callback to update ProgressBar
        $context = stream_context_create($arrContextOptions, array('notification' => function ($notification_code, $severity, $message, $message_code, $bytes_transferred, $bytes_max) use ($output, $progress) {
            switch ($notification_code) {
                case STREAM_NOTIFY_RESOLVE:
                case STREAM_NOTIFY_AUTH_REQUIRED:
                case STREAM_NOTIFY_COMPLETED:
                case STREAM_NOTIFY_FAILURE:
                case STREAM_NOTIFY_AUTH_RESULT:
                    /** Ignore That */
                    break;

                case STREAM_NOTIFY_REDIRECTED:
                    $output->writeln("Redirect To : ", $message);
                    break;

                case STREAM_NOTIFY_CONNECT:
                    $output->writeln("Connected...");
                    break;
                case STREAM_NOTIFY_FILE_SIZE_IS:
                    /** @var $progress ProgressBar */
                    $output->writeln("Downloading...");
                    $progress->start($bytes_max);
                    break;
                case STREAM_NOTIFY_PROGRESS:
                    $progress->setProgress($bytes_transferred);
                    break;
            }
        }));

        try {
            //Download file
            $streamContent = file_get_contents($this->url, false, $context);
            $progress->finish();

            //Save File
            file_put_contents($archiveDestPath . self::XML_FILE_NAME, $streamContent);
        } catch (\Exception $ex) {
            throw new XMLFileDownloadException(self::XML_FILE_NAME);
        }

    }
}

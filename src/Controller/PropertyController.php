<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Services\GetProperties;
use App\Services\GetPropertiesService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Domain\DataTransferObject\FilterParametersDTO;

class PropertyController extends Controller
{
    private const FILE_NAME = 'downloads/properties.json';

    /**
     * @var GetPropertiesService
     */
    private $getPropertiesService;

    public function __construct(GetProperties $getPropertiesService)
    {
        $this->getPropertiesService = $getPropertiesService;
    }

    public function listPropertiesAction(Request $request): JsonResponse
    {
        $pageNumber = $request->query->getInt('page');
        $sortBy = $request->query->get('sortBy');

        $filterParameters = new FilterParametersDTO($sortBy, $pageNumber);
        $properties = $this->getPropertiesService->execute($filterParameters);

        return $this->json($properties);
    }

    public function downloadAction(Request $request): BinaryFileResponse
    {
        $pageNumber = $request->query->getInt('page');
        $sortBy = $request->query->get('sortBy');

        $filterParameters = new FilterParametersDTO($sortBy, $pageNumber);
        $properties = $this->getPropertiesService->execute($filterParameters);

        $this->createFileFromJsonData(json_encode($properties));
        return $this->file(self::FILE_NAME);

    }

    private function createFileFromJsonData(string $jsonData): void
    {
        $file = fopen(self::FILE_NAME, 'w');
        fwrite($file, $jsonData);
        fclose($file);
    }
}
<?php
declare(strict_types=1);

namespace App\Services;


use App\Domain\DataTransferObject\FilterParametersDTO;
use App\Repository\PropertyRepository;
use App\Domain\ValueObject\SortBy;

class GetPropertiesService implements GetProperties
{
    /**
     * @var PropertyRepository
     */
    private $propertyRepository;

    public function __construct(PropertyRepository $propertyRepository)
    {
        $this->propertyRepository = $propertyRepository;
    }

    public function execute(FilterParametersDTO $filterParametersDTO): array
    {
        $sortBy = SortBy::create($filterParametersDTO->getSortBy());

        return $this->propertyRepository->filter(
            $sortBy,
            $filterParametersDTO->getPageNumber(),
            $filterParametersDTO->getPageSize()
        );
    }
}
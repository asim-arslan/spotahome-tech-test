<?php
declare(strict_types=1);

namespace App\Services;


use App\Domain\Entity\Property;
use App\Repository\PropertyRepository;

class ImportPropertiesService implements ImportProperties
{
    /**
     * @var PropertyRepository
     */
    private $propertyRepository;

    public function __construct(PropertyRepository $propertyRepository)
    {
        $this->propertyRepository = $propertyRepository;
    }

    public function execute(string $url): bool
    {
        $rawPropertiesData = file_get_contents($url);
        $properties = $this->getPropertiesFromRawData($rawPropertiesData);

        return $this->propertyRepository->save($properties);
    }

    private function getPropertiesFromRawData(string $rawPropertiesData): array
    {
        $propertiesXML = new \SimpleXMLElement($rawPropertiesData);

        $properties = [];
        foreach ($propertiesXML as $property) {
            $id = (int) $property->id;
            $title = (string) $property->title;
            $city = (string) $property->city;
            $link = (string) $property->url;

            $image = (string) (isset($property->pictures->picture[0]->picture_url) ? $property->pictures->picture[0]->picture_url :"");

            $properties[] = Property::create($id, $title, $link, $city, $image);
        }

        return $properties;
    }
}
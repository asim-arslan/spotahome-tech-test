<?php
declare(strict_types=1);

namespace App\Services;


interface ImportProperties
{
    public function execute(string $url): bool;
}
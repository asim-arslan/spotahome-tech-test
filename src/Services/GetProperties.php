<?php
declare(strict_types=1);

namespace App\Services;

use App\Domain\DataTransferObject\FilterParametersDTO;

interface GetProperties
{
    public function execute(FilterParametersDTO $filterParametersDTO): array;
}

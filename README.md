# SPOTAHOME Technical Challenge

**Requirements**
1. composer
2. php 7.2

**How to setup:**

1. Unzip source code / or git clone repository
2. CD to project directory and install dependencies
    > **composer install**
3. Create Database:
- make sure you set DB username and password in .env file
    > **php bin/console --env=test doctrine:database:create --if-not-exists**   
3. Run Migrations: 
    > **php bin/console doctrine:migrations:migrate** 
4. Import Properties XML Data: 
    > **php bin/console app:properties:import**        
5. Run command: 
    > **php bin/console server:run 0.0.0.0:8000**
6. Symfony development server started 
    > http://localhost:8000

